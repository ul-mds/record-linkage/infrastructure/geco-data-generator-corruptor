# GeCo Data Generator and Corruptor

This is the original source code of the GeCo Data Generator and Corruptor.
A limited feature set and the source code is [available on the website of the Australian National University](https://dmm.anu.edu.au/geco/).
No modifications have been done to the source code.
The MPL license file has been moved to [LICENSE](./LICENSE) and this README has been added to provide context.
This copy of the source code has been created for archival purposes.

## Requirements

This project requires Python 2.7.

## References

- K.-N. Tran, D. Vatsalan, and P. Christen, “GeCo: an online personal data generator and corruptor,” in Proceedings of the 22nd ACM international conference on Information & Knowledge Management, New York, NY, USA, Oct. 2013, pp. 2473–2476. doi: 10.1145/2505515.2508207.
- P. Christen and D. Vatsalan, “Flexible and extensible generation and corruption of personal data,” in Proceedings of the 22nd ACM international conference on Information & Knowledge Management, New York, NY, USA, Oct. 2013, pp. 1165–1168. doi: 10.1145/2505515.2507815.